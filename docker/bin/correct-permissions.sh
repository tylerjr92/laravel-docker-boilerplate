#!/usr/bin/env sh

#source "$(dirname $0)/../../.env"

find ./ -type d -print0 | xargs -0 chmod 0755

find ./ -type f -print0 | xargs -0 chmod 0644

chmod -R 775 ./storage

find ./bootstrap/ -type d | xargs -0 chmod 0775