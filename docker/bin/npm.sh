#!/usr/bin/env sh

source "$(dirname $0)/../../.env"

docker exec -it "$APP_IMAGE_NAME"-php-fpm npm $*