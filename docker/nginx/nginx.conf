# For more information on configuration, see:
#   * Official English Documentation: http://nginx.org/en/docs/
#   * Official Russian Documentation: http://nginx.org/ru/docs/

daemon off;
error_log /dev/stdout;
pid /tmp/nginx.pid;
worker_processes auto;
worker_rlimit_nofile 65535;

# Load dynamic modules. See /usr/share/nginx/README.dynamic.
include /usr/share/nginx/modules/*.conf;

events {
    multi_accept on;
    worker_connections 65535;
}

http {
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log   /dev/stdout  main;

    charset utf-8;
    sendfile on;
    tcp_nopush on;
    tcp_nodelay on;
    server_tokens off;
    log_not_found off;
    types_hash_max_size 2048;
    client_max_body_size 16M;

    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;

    # Load modular configuration files from the /etc/nginx/conf.d directory.
    # See http://nginx.org/en/docs/ngx_core_module.html#include
    # for more information.

#   Force http to https
    server {
        listen 8080 default_server;
        server_name _;
        location /ngx-alive {return 200 'HTTP OK';}
        # Destination redirect base URI
        location / {return 301 https://$host$request_uri;}
    }

    server {
        listen 8443 ssl default_server;
        ssl_certificate /etc/nginx/ssl/CHANGE_ME.crt;
        ssl_certificate_key /etc/nginx/ssl/CHANGE_ME.key;

        ssl_protocols TLSv1.2 TLSv1.3;
        ssl_prefer_server_ciphers on;
        ssl_dhparam /etc/nginx/ssl/dhparam.pem;
        ssl_ciphers ECDHE-RSA-AES256-GCM-SHA512:DHE-RSA-AES256-GCM-SHA512:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-SHA384;
        ssl_ecdh_curve secp384r1; # Requires nginx >= 1.1.0
        ssl_session_timeout  10m;
        ssl_session_cache shared:SSL:10m;
        ssl_session_tickets off; # Requires nginx >= 1.5.9
        resolver 8.8.8.8 8.8.4.4 valid=300s;
        resolver_timeout 5s;
        add_header Strict-Transport-Security "max-age=MAXAGEHSTS; includeSubDomains; always";
        add_header X-Frame-Options DENY;
        add_header X-Content-Type-Options nosniff;
        add_header X-XSS-Protection "1; mode=block";

        server_name CHANGE_ME www.CHANGE_ME;

        root /app/public;
        index index.php index.html;

        charset utf-8;

        location /ngx-alive {return 200 'HTTPS OK';}

        location / {
            try_files $uri $uri/ /index.php?$args;
        }

        location = /favicon.ico { access_log off; log_not_found off; }

        location = /robots.txt  { access_log off; log_not_found off; }

        location ~ \.php$ {
            try_files $uri =404;
            fastcgi_split_path_info ^(.+\.php)(/.+)$;
            fastcgi_pass php-fpm:9000;
            fastcgi_index index.php;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            include fastcgi_params;
            fastcgi_buffers 16 256k;
            fastcgi_buffer_size 256k;
            fastcgi_send_timeout 1200s;
            fastcgi_read_timeout 1200s;
            proxy_connect_timeout 1200s;
            proxy_send_timeout 1200s;
            proxy_read_timeout 1200s;
        }

        access_log  /dev/stdout;
        error_log  /dev/stdout;
    }
}