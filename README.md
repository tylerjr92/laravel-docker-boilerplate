# Laravel Docker Bootstrap Project

### Designed and Maintained by: Tyler J. Radlick

## About this project
This is just the bare framework to allow for a production or development style docker deployment with Laravel.

This repo attempts to follow best practices for Docker, Laravel, Composer, and NPM in production environments. With that said, this is my living interpretation of these best practices mixed with some strong opinions on how I like to run my Laravel applications.

I welcome any contributors who wish to join in. This project was born out of my frustration of maintaining different docker configurations and deployments combined with the amount of time it takes to start a new project.

Lastly this project is still under active development. As such you may have some issues. I would greatly appreciate any issues opened against the project so I can perfect it for everyone.

Merge Requests are even more appreciated.

Specific areas that need work still:

* [ ] Documentation
* [ ] Browsersync support
* [ ] Fix xDebug support

## What this project assumes
Again this project is based on how I run my production deployments in Docker. As such some of what is expected or built in may need to be removed for you.

This project assumes you are using it alongside a laravel Application.

This requires you to add values to your .env file:
```
########################
#     App Settings     #
########################
APP_NAME=Laravel
APP_IMAGE_NAME=docker-bs
APP_DOMAIN=localhost
APP_PHP_VERSION=8.1
APP_HTTP_PORT=8080
APP_HTTPS_PORT=8443

########################
#   Database Settings  #
########################
DB_PORT=3306
DB_DATABASE=app
DB_USERNAME=app
DB_PASSWORD=app
# PHPUnit Test DB
DOCKER_DEV_MYSQL_TEST_PORT=3307
DB_DATABASE_TEST=app
DB_USERNAME_TEST=app
DB_PASSWORD_TEST=app

########################
#    Docker Settings   #
########################
DOCKER_REGISTRY_URL=docker.io
DOCKER_REGISTRY_PREFIX=tylerjr92/laravel-docker-boilerplate
DOCKER_IMAGE_TAG=latest
DOCKER_LOCAL_CODE_DIR=./
DOCKER_DEV_MYSQL_PORT=3306
DOCKER_DEV_REDIS_PORT=6397
COMPOSE_PROJECT_NAME=LOCAL_DEV
CI_JOB_ID=tr_local
XDEBUG_ENABLED=FALSE
CUID=501
CGID=501
REDIS_IMAGE=bullseye

########################
#    Redis Settings    #
########################
REDIS_PASSWORD=null
REDIS_PORT=6379

########################
#   Mailhog Settings   #
########################
MAIL_PORT=2525

########################
# BrowserSync Settings #
########################
MIX_BS_PORT=3000

```

This project assumes this directory structure:
```
LaravelApplicationFolder/
├── CONTRIBUTING.md
├── K8s
│   ├── Infra
│   │   ├── cert-manager-acme-prod-solver.yaml
│   │   ├── cert-manager-acme-staging-solver.yaml
│   │   ├── cert-manager-selfsigned.yaml
│   │   ├── gitlab-admin-service-account.yaml
│   │   ├── gitlab-registry-secret.yaml
│   │   ├── gitlab-runner-values.yaml
│   │   └── setup-cluster.sh
│   ├── bin
│   │   └── publish_Helm_templates.sh
│   ├── charts
│   │   └── laravel-k8s-app
│   │       ├── Chart.yaml
│   │       ├── templates
│   │       │   ├── _helpers.tpl
│   │       │   ├── cron-deployment.yaml
│   │       │   ├── gitlab-registry-secret.yaml
│   │       │   ├── horizon-cpu-hpa.yaml
│   │       │   ├── horizon-deployment.yaml
│   │       │   ├── ingress.yaml
│   │       │   ├── laravel-env.yaml
│   │       │   ├── Nginx-configmap.yaml
│   │       │   ├── Nginx-cpu-hpa.yaml
│   │       │   ├── Nginx-deployment.yaml
│   │       │   ├── Nginx-service.yaml
│   │       │   ├── PHP-FPM-cpu-hpa.yaml
│   │       │   ├── PHP-FPM-deployment.yaml
│   │       │   ├── php-service.yaml
│   │       │   ├── phpfpmpool-configmap.yaml
│   │       │   └── phpini-configmap.yaml
│   │       └── values.yaml
│   └── rendered
│       └── laravel-k8s
│           └── laravel-k8s-app
│               └── templates
│                   ├── cron-deployment.yaml
│                   ├── gitlab-registry-secret.yaml
│                   ├── horizon-cpu-hpa.yaml
│                   ├── horizon-deployment.yaml
│                   ├── ingress.yaml
│                   ├── laravel-env.yaml
│                   ├── Nginx-configmap.yaml
│                   ├── Nginx-cpu-hpa.yaml
│                   ├── Nginx-deployment.yaml
│                   ├── Nginx-service.yaml
│                   ├── PHP-FPM-cpu-hpa.yaml
│                   ├── PHP-FPM-deployment.yaml
│                   ├── php-service.yaml
│                   ├── phpfpmpool-configmap.yaml
│                   └── phpini-configmap.yaml
├── LICENSE
├── README.md
├── docker
│   ├── bin
│   │   ├── artisan.sh
│   │   ├── bash.sh
│   │   ├── composer.sh
│   │   ├── correct-permissions.sh
│   │   ├── flush-redis.sh
│   │   ├── horizon-start.sh
│   │   ├── horizon-stop.sh
│   │   ├── npm.sh
│   │   ├── publish-docker-images-for-all-versions.sh
│   │   ├── remove-all-containers.sh
│   │   ├── remove-all-docker-images.sh
│   │   ├── test-with-coverage.sh
│   │   └── test.sh
│   ├── mysql
│   │   └── my.cnf
│   ├── Nginx
│   │   ├── Nginx.Dockerfile
│   │   └── Nginx.conf
│   ├── PHP-FPM
│   │   ├── crontab
│   │   ├── PHP-FPM-pool.conf
│   │   ├── PHP-FPM.Dockerfile
│   │   ├── php7.4.ini
│   │   ├── php8.0.ini
│   │   ├── php8.1.ini
│   │   └── supervisor.conf
│   ├── redis
│   │   ├── redis.Dockerfile
│   │   └── redis.conf
│   ├── runConfigurations
│   │   ├── Development - No GUIs.run.xml
│   │   ├── Development.run.xml
│   │   ├── Nginx.run.xml
│   │   ├── PHP-FPM.run.xml
│   │   └── redis.run.xml
│   └── volumes
│       ├── mysql
│       │   ├── data
│       │   └── seed
│       ├── Nginx
│       ├── Nginx-proxy
│       ├── redis
├── docker-compose-k8s.yaml
├── docker-compose.yaml
├── production.yaml
└── reverse-proxy.yaml
```

## Getting Started
1. Download the files in this repo and copy them into your Laravel application
2. Copy the values needed from .env.example to your laravel .env file
3. Start the docker compose file by running `docker-compose -f docker-compose.yaml up -d`
4. View your services on the corresponding ports

If you are having an issue with any of the services in the app server, logs can be found in the `/logs` directory.

Application code can be found in the `/app` directory.

## WHERE'S LARAVEL?
If you just clone this repo and run docker-compose up you will notice everything falls apart. This is a bootstrap project and it is designed to be added to an already existing Laravel application.

I do not include a sample Laravel application because it will work with all versions of Laravel 6+ and keeping it updated would reduce the amount of time I have to focus on keeping the Docker side maintained :D

If you do not have a current Laravel app, checkout the [Docs](https://laravel.com) to get a project going.

Once you have your base project, copy the files from this repo into the main project folder and enjoy.

## Development
Your base project is bound to the Docker containers using the delegated method for speed.

You also should run all artisan, composer, and NPM commands in the PHP-FPM container.

To help with this, I have included a few scripts in the docker/bin folder [Credit: Aaron Saray](https://github.com/package-for-laravel/new-project)

For example, to run `php artisan config:cache` you can simple run `./docker/bin/artisan.sh config:cache`

You can replace config:cache with any artisan command including custom ones.

Another example would be `composer install`, use `./docker/bin/composer.sh install`

Running these commands in the containers the code is running in ensures your files are always compiles with the versions of composer, NPM, node, and PHP as you expect.

This also prevents you from having to configure your local versions of composer, NPM, node, and PHP.

You could achieve the same thing running `docker exec -it container_name command` but this is much faster in the long run.

Completely optional but I do suggest using this alias's for an even easier time developing in docker. Add the below to your .zshrc or .bashrc file:
```
# Docker Artisan
alias dart=./docker/bin/artisan.sh

# Docker NPM
alias dnpm=./docker/bin/npm.sh

# Docker Composer
alias dcom=./docker/bin/composer.sh

# Quickly open a bash shell into PHP-FPM
alias dsh=./docker/bin/bash.sh

# Quickly flush redis
alias flushrdb=./docker/bin/flush-redis.sh

```

## Production
You can deploy your production application by running `docker-compose -f production.yaml up -d`

A few words of caution:
1. I am still testing this production deployment, although this project is solid for development work, more work is required for a satisfactory production deployment.
2. You should have a solid understanding of how Docker works before you move forward using this in production.
3. Redis and MySQL are included in the production.yaml compose file. This is for convince however it is a horrible idea if you care at all about the data stored in either database.
4. Redis and MySQL persist data to the docker/volumes/ folder. If you care at all about the data in your databases, do not delete this folder ever and back it up.
5. Currently, the packaged code is stored in PHP-FPM, but Nginx, Horizon, and Cron all need access to it. This has not been solved and as such the packaged code is overwritten at runtime with a local copy of the code.
5. You were warned, have fun. `^__^`

All that aside, the production.yaml compose file will tag the latest versions of all your container's images with the prod tag.

You can push these final images to a docker repo by using the docker commit command.

You can also use the built in helper in docker/bin/tag-and-push-production-images.sh.

The command will config:cache laravel, composer install, NPM install, npm run prod, commit each image, tag it using the env variables from the Docker section, and push them to your registry.

Further compilation may be required for your application and as such you should add the stages in the tag-and-push-production-images.sh file before using it.

As a best practice, code should not be changing once you have your production environment.

These final images include everything in the container at the time they were tagged, this includes your app code.

You can then push these committed images to a public or private docker repo and use them on your production server.

This is especially useful for K8's style deployments.

### Bundle for production guide
1. Stop your development stack version by running `docker-compose -f docker-compose.yaml down`
2. Run `docker-compose -f production.yaml up -d`
3. Wait for them to all start
4. Run `./docker/bin/tag-and-push-production-images.sh` to compile, commit, and push your final build to your docker registry

Note: This likely is best done in CI/CD however I include this sample file for those who don't have access to or are not knowledgeable with CI/CD.

## Production Server
You should only deploy the reverse-proxy.yaml.yaml file's Nginx-proxy and Nginx-proxy-le service once per host.

This docker-compose stack must be deployed and be running before production.yaml is brought up.

This docker compose fill will run locally, but you will likely have issues unless you are using some sort of local DNS service or host file.

It is also setup for a production environment rather than a local environment.

This service binds to docker and listens for when a container starts.

It then creates a reverse proxy config based on your APP_DOMAIN.

This may seem redundant but this is done to allow you to deploy many webservers on a single server.

All of the other containers must not share ports. For example:
```
site1/Nginx/latest binds port 8000 on the server to port 8080 on the container
site2/Nginx/latest binds port 8001 on the server to port 8080 on the container
```
Because of the reverse proxy, both webservers are available on a single IP address, they can have completely different domains, and even use different images and services.

The reverse proxy matches your domain on the host(server) and serves the traffic on port 80 and 443.

Your applications webserver needs to have a few environment variables to glue everything together. In docker-compose define these variables to tell dockergen to make a Nginx config for your container:
```
environment:
      - VIRTUAL_HOST=${APP_DOMAIN} #Publically available DNS FQDN
      - VIRTUAL_PROTO=https #Should the reverse proxy connect via http or https?
      - VIRTUAL_PORT=8443 #What port is https running on in the continer
```

You can also have the reverse-proxy.yaml.yaml compose file configure a free SSL certificate for your containers as well, simply specify `LETSENCRYPT_HOST=${APP_DOMAIN} #Publicly available DNS FQDN` to have the certificate generated.

NOTE: If your applications domain name is not viewable by the internet, lets encrypt will fail to generate a certificate, and the reverse proxy may have trouble proxying your requests.

Lets encrypt is not required and will not attempt to make a certificate if you do not define the environment variable.

You can read the docs for Nginx-proxy/Nginx-proxy here: [Docs](https://github.com/Nginx-proxy/Nginx-proxy)

You can read the docs for Nginx-proxy/docker-letsencrypt-Nginx-proxy-companion here: [Docs](https://github.com/Nginx-proxy/docker-letsencrypt-Nginx-proxy-companion)

Caveat's:
* If you do not provide the reverse proxy container with a valid certificate, it will refuse to connect on port 443
* All of the data for Nginx-reverse proxy and lets encrypt are removed when you run `docker-compose -f reverse-proxy.yaml.yaml down -v` This should not be an issue unless you deploy many times as lets encrypt does have a throttling mechanism
* If the throttling is an issue or concern for you, you should create a bind mount and persist the volumes for the above containers

## File Explanation
.env stores key value pairs used for building the environment.

docker-compose.yaml is a docker compose stack containing:
```
Nginx
PHP-FPM
Horizon
Cron
Redis
MySQL
MySQL-Test
MailHog
Selenium
```

production.yaml is a docker compose stack containing:
```
Nginx
PHP-FPM
Horizon
Redis
MySQL
```

reverse-proxy.yaml.yaml is a docker compose stack that pulls its images from your specified repo using the latest tag. It contains the below services:
```
Nginx-proxy
letsencrypt
```

##### About the other files
The crontab file is read by supercronic to create the cron definitions. You can make as many cron definitions as you like with one per line.

Nginx.conf is the Nginx configuration file for the app server.

the php7.4.ini, 8.0, and 8.1 files are settings files for PHP. You can adjust these files to tune PHP for your application.

PHP-FPM-pool.conf is the PHP-FPM configuration file. These settings likely should be left as is but can be tuned if you find the need to.

supervisor.conf is the Supervisord configuration file. It defines all the processes that are started and where the logs for them go.

redis.conf is the entire configuration for Redis. You can fine turn your redis environment here however you like.

## Architecture
Every service is separated out into its own container.

This follows the Docker best practice to only have one running process as well as mimics an ideal production deployment.

When using K8's, you want every service in its own container so they can scale independently of one another.

### Build Definitions
All services are extended from their base version in hub.docker.com

For example, PHP-FPM is extended from the official php:7.*-fpm docker hub image.

#### PHP-FPM
This dockerfile is used to generate the development, horizon, cron, and production environments.

This is accomplished using multi-stage build definitions.

Production used the build stage.

Development adds xdebug. Otherwise, it is identical to production.

The reason horizon and cron are based off the build stage of PHP-FPM is because they require an identical PHP environment to run the app.

If you do not require cron or horizon you can remove them from the docker-compose.yaml and production.yaml files.

#### Nginx and Redis
Nginx and Redis only have one build stage and are production ready out of the gate.

They both add the appuser and reduce privilege before running their corresponding process.

Redis persists data in development to docker/volumes/redis

If you care about the data in Redis, you should setup a similar folder outside of the app in production for Redis to persist to.

If you do not, when you rebuild the Redis container, you will lose all of the data in the Redis database.

### Explanation of services
#### Process Management
Supervisor is used to keep horizon running. [Docs](http://supervisord.org/)

#### Cron
Cron is handled by supercronic. [Docs](https://github.com/aptible/supercronic)

#### Webserver
Nginx is used as the webserver. [Docs](https://Nginx.org/en/docs/)

A self-signed certificate is generated for you based on the domain you specify.

You may optionally choose to replace this in production

#### Webserver PHP support
PHP-FPM is running alongside Nginx. [Docs](https://www.php.net/manual/en/install.fpm.configuration.php)

Nginx proxies php requests to PHP-FPM using docker DNS.

#### PHP Configuration
PHP is installed from [Offical PHP](https://hub.docker.com/_/php)

Modules are installed from [mlocati/docker-php-extension-installer](https://github.com/mlocati/docker-php-extension-installer)

Note: xdebug is only installed in the development stage

You can install any modules from that repo however these PHP modules are installed by default:
```
bcmath
Core
ctype
curl
date
dom
fileinfo
filter
ftp
gd
hash
iconv
imap
intl
json
ldap
libxml
mbstring
mcrypt
mysqli
mysqlnd
openssl
pcntl
pcre
PDO
pdo_mysql
pdo_sqlite
Phar
posix
readline
redis
Reflection
session
SimpleXML
soap
sodium
SPL
sqlite3
standard
tidy
tokenizer
xml
xmlreader
xmlrpc
xmlwriter
Zend OPcache
zip
zlib

[Zend Modules]
Zend OPcache
```

### Kubernetes
Requires Helm V3!

This section is by no means a how to use Kubernetes. I expect you have some basic understanding of Kubernetes and Helm. If you do not, there are great tutorials out there and I suggest completing a few of them before attempting this.

I have included a basic Helm chart for a laravel app. This will function properly in development and production.

Bin contains a script to help you render the Helm files for convenience. You should not attempt to deploy the rendered files, they are there mostly for troubleshooting. Instead use Helm to deploy the application after pushing it to a Helm repo.

The charts folder contains the Helm chart laravel-k8s-app. In this chart you will find the templates. Be sure to remove the laravel-env.yaml file before you deploy. This file should be stored securely and copied in from CI/CD during deployment and not checked into source control. Checking this file into source control will expose your secrets because they are just base64 encoded ;).

Each docker container has its own deployment. Each deployment contains the steps to prepare each container. This is required because Kubernetes may scale up and down containers at will so doing these steps in CI/CD is not suggested.

I also setup some basic HPAs which will help Kubernetes determine if it should scale a pod up or down.

The ingress included supports 1 or 2 domains going to the same app. More domains can be added by modifying the ingress.yaml and values.yaml file.

laravel-env.yaml contains your .env entries. Do take note that they must be in yaml json format and not in key=value format. Also note that all the values must be base64 encoded or they will not function properly.

The Nginx-configmap contains the Nginx.conf file that is loaded by Nginx. This needs to be updated to support your configuration but will work as is for most deployments.

The phpini-config contains the php.ini file used to tune PHP. It needs to be updated with the version specific ini file you are using in Docker. It also contains sane defaults as suggested by PHP but likely needs tweaking by some.

phpfpmpool-configmap contains the PHP FPM pool configuration. Likewise, these defaults are mostly safe and sane but you may wish to update it to match your needs.

There are two services that are exposed, Nginx and PHP-FPM. These expose the ports required in Kubernetes for everything to talk to each other. Do note that you should NOT expose PHP-FPM's port to the outside world. It should only be accessed by Nginx for proxying php requests.

gitlab-registry-secret.yaml is provided for convince to show how a docker login secret can be included. If you do not need it, delete it. It goes without saying again, but none of your secrets should actually be committed to source control. Inject this file via CI/CD in order to keep your container images safe.

Helm using the Go template engine. The template files are populated with values from values.yaml. You will want to review these values and make modifications as required.

If you wish to change the name of the Helm chart you need to rename the folder laravel-k8s-app and change the name in Chart.yaml. They must match in order for Helm to link everything properly.

You can render new template files by running `./K8s/bin/publish_Helm_templates.sh`

You can lint your Helm chart by running `helm lint K8s/charts/laravel-k8s-app`

Install the Helm chart by running `helm upgrade --install --debug laravel-k8s K8s/charts/laravel-k8s-app --values K8s/charts/laravel-k8s-app/values.yaml --namespace laravel-k8s --create-namespace --wait --timeout 15m`

You can add additional arguments to the above command in order to set values via the command in values.yaml. Add `--set key=value`

If you think it's odd that Helm suggests using the upgrade --install syntax, so did I! Basically what this means is we are going to upgrade the current app but if it has not been installed yet, then we will install it first. It is a catchall to prevent CI/CD errors and mistakes. In production, you likely shouldn't use --debug but while you are getting things setup it really helps you understand what Helm is doing. Without it, it looks like Helm is doing nothing until it suddenly fails or succeeds. The downside is it will print all of your templates at the end of the deployment, exposing your secrets in your CI/CD build output. Not ideal.

The infra folder contains some of the Kubernetes resources I need in my environments before I deploy my Helm apps. Minimally you will need to deploy the cert manager solvers and install the Nginx ingress controller for the app to function properly. This can be changed but would require you to modify the chart templates. The Gitlab resources are not required but are helpful if you are using Gitlab.

### GitLab CI/CD Example Pipeline
Coming Soon!

### Security
The Dockerfile is set to create a non privileged user named appuser. This user runs the services in the resulting image as well as has rights over the application files in the image.

The only exception to the appuser security user is in development. MailHog currently requires root access as it binds to port 25

#### Security Issues reporting request:
Should you find any security vulnerabilities, I ask that you please email and provide 90 days to resolve before making a public issue. This is out of regard to the number of applications I and others may have running this. I will do my best to patch any vulnerabilities that are a result of how I use the above technology. I am in no way able to path vulnerabilities with Docker or Kubernetes and would encourage you to notify them properly as well.